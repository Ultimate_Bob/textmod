/**
 * Enum representing a specific type of converter.
 * @readonly
 * @enum {string}
 */
const ConverterType = {
    Converter: 'Converter',
    LineConverter: 'LineConverter',
    WordConverter: 'WordConverter',
};

/** 
 * A converter function accepting string returning a converted string. 
 * @typedef {(text: string) => string} ConverterFunc
 */

/**
 * A converter function definition containing additional metadata about a converter.
 */
class Converter {
    /** @type {ConverterFunc} */
    func;
    /** @type {ConverterType} */
    type;
    /** @type {string | undefined} */
    desc;

    /**
     * @param {ConverterFunc} func 
     * @param {ConverterType} type 
     * @param {string | undefined} desc 
     */
    constructor(func, type, desc) {
        this.func = func;
        this.type = type;
        this.desc = desc;
    }
}

/** 
 * A line converter function accepting line and number returning a converted line.
 * @typedef {(line: string, index: number) => string} LineConverterFunc
 */

/** 
 * A word function accepting word and number returning a converted word.
 * @typedef {(word: string, index: number) => string} WordConverterFunc
 */

/** 
 * Options passed to word conversion.
 * @typedef {{
 *      excludeFirst?: boolean;
 *      excludeLast?: boolean;
 * }} WordConverterOptions
 */

/** 
 * The list of all converters.
 * @type {Converter[]} 
 */
var converters = [];

function createConverters() {
    converters = [

        new Converter(IncrementingCDefines, ConverterType.LineConverter, IncrementingCDefines_Desc),
        new Converter(IncrementingCSharpEnum, ConverterType.LineConverter, IncrementingCSharpEnum_Desc),
        new Converter(TakeFirstLetter, ConverterType.LineConverter, TakeFirstLetter_Desc),
        new Converter(TakeLastLetter, ConverterType.LineConverter, TakeLastLetter_Desc),
        new Converter(CommaSeparatedWords, ConverterType.WordConverter, CommaSeparatedWords_Desc)
    ]
};


// =================================== SYSTEM ===================================

/**
 * @param {string} text
 * @param {Converter} converter
 * @returns {string}
 */
function convertText(text, converter) {
    return converter.func(text);
}

/**
 * @param {string} text 
 * @param {LineConverterFunc} lineConversion 
 * @returns {string}
 */
function convertLines(text, lineConversion) {
    var lines = text.split("\n");
    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        lines[i] = lineConversion(line, i);
    }
    return lines.join('\n');
}

/**
 * @param {string} text 
 * @param {WordConverterFunc} wordConversion 
 * @param {WordConverterOptions} options
 * @returns {string}
 */
function convertWords(text, wordConversion, options = undefined) {
    var words = text.split(" ");
    for (var i = 0; i < words.length; i++) {
        if (i == 0 && options?.excludeFirst)
            continue;
        if (i == words.length - 1 && options?.excludeLast)
            continue;

        var line = words[i];
        words[i] = wordConversion(line, i);
    }
    return words.join('');
}

/**
 * Runs the currently selected converter.
 */
function runConverter() {
    var converter = getConverter();
    if (!converter) return;

    /** @type {HTMLTextAreaElement} */
    var inputEl = document.getElementById("in");
    /** @type {HTMLTextAreaElement} */
    var outputEl = document.getElementById("out");
    var inText = inputEl.value;
    var outText = convertText(inText, converter);
    outputEl.value = outText;
}

function setElText(id, text) {
    var el = document.getElementById(id);
    el.textContent = text;
}

function clearOutput() {
    /** @type {HTMLTextAreaElement} */
    var outputEl = document.getElementById("out");
    outputEl.value = "";
}

/**
 * Generates a pair of before+after placeholders for a converter.
 * @param {Converter} converter 
 */
function generatePlaceholderText(converter) {
    var input = "This is an example sentence.";
    if (converter.type == ConverterType.LineConverter) {
        input = `FirstItem
SecondItem
ThirdItem
FourthItem
FifthItem
SixthItem`;
    }
    else if (converter.type == ConverterType.WordConverter) {
        input = "FirstItem SecondItem ThirdItem FourthItem FifthItem SixthItem";
    }

    const output = converter.func(input);

    /** @type {HTMLTextAreaElement} */
    var inputEl = document.getElementById("in");
    inputEl.placeholder = input;
    /** @type {HTMLTextAreaElement} */
    var outputEl = document.getElementById("out");
    outputEl.placeholder = output;
}

function onConverterChanged() {
    var converter = getConverter();
    if (!converter) return;

    setElText("converter-type", converter.type)
    setElText("converter-desc", converter.desc)

    generatePlaceholderText(converter);
}

/**
 * Gets a converter.
 * @param {string | undefined} name Name of converter to get. If undefined, then the current value in the selector element is used. 
 */
function getConverter(name) {

    if (name == undefined) {
        /** @type {HTMLSelectElement} */
        var converterSelectEl = document.getElementById("converter-select");
        name = converterSelectEl.value;
    }

    var converter = converters.find(c => c.func.name == name)
    if (converter == null) {
        console.error("Converter not found.");
        return;
    }
    return converter;
}

function populateConverterSelectOptions() {
    /** @type {HTMLSelectElement} */
    var select = document.getElementById('converter-select');

    for (var conv of converters) {
        var convName = conv.func.name;
        var option = document.createElement('option');
        option.value = convName;
        option.innerText = convName;
        select.appendChild(option);
    }

    select.selectedIndex = 0;
}

function setupEventHandlers() {
    /** @type {HTMLSelectElement} */
    var select = document.getElementById('converter-select');
    select.addEventListener('change', onConverterChanged);
}

function load() {
    createConverters();
    setupEventHandlers();
    populateConverterSelectOptions();
    onConverterChanged()
}

// =================================== CONVERTERS ===================================

const IncrementingCDefines_Desc = "Converts a list of strings into C defines with a value equal to their line index."
/** @type {ConverterFunc} */
function IncrementingCDefines(text) {
    return convertLines(text, (line, i) => {
        var name = line
            .trim()
            .replace(',', '');
        return `#define ${name} ${i}`;
    });
}

const IncrementingCSharpEnum_Desc = "Converts a list of strings into C# enum values equal to their line index. This is encased in an enum declaration."
/** @type {ConverterFunc} */
function IncrementingCSharpEnum(text) {
    var list = convertLines(text, (line, i) => {
        var name = line
            .trim()
            .replace(',', '');
        return `    ${name} = ${i},`;
    });
    return `public enum MyEnum
{
${list}
}
    `;
}

const TakeFirstLetter_Desc = "Takes the first letter of each line."
/** @type {ConverterFunc} */
function TakeFirstLetter(text) {
    return convertLines(text, (line, i) => {
        if (line.length == 0)
            return '';
        return line.charAt(0);
    });
}

const TakeLastLetter_Desc = "Takes the last letter of each line."
/** @type {ConverterFunc} */
function TakeLastLetter(text) {
    return convertLines(text, (line, i) => {
        if (line.length == 0)
            return '';
        return line.charAt(line.length - 1);
    });
}

const CommaSeparatedWords_Desc = "Separates each word with a comma."
/** @type {ConverterFunc} */
function CommaSeparatedWords(text) {
    return convertWords(text, (word, i) => {
        if (word.length == 0)
            return '';
        return `${word}, `;
    }, { excludeLast: true });
}




// =================================== LOAD ===================================
function onBodyLoaded() { load(); }